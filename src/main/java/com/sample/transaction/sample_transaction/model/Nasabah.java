package com.sample.transaction.sample_transaction.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Table(name = "nasabah")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Nasabah {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false,length = 100)
    private Long id;
    @Column(name = "name", nullable = false,length = 100)
    private String name;
    @Column(name = "no_rekening", nullable = false,length = 100)
    private String norek;
    @Column(name = "Saldo", nullable = false,length = 100)
    private Double amount;
    @Column(name = "Waktu")
    @DateTimeFormat(pattern = "dd-mm-yyyy")
    private LocalDateTime date;
    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
