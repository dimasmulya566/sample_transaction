package com.sample.transaction.sample_transaction.service;

import com.sample.transaction.sample_transaction.dto.NasabahRequest;
import com.sample.transaction.sample_transaction.dto.NasabahResponse;
import com.sample.transaction.sample_transaction.model.Nasabah;
import com.sample.transaction.sample_transaction.repo.NasabahRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NasabahServiceImpl implements NasabahService{

    @Autowired
    private NasabahRepo nasabahRepo;

    @Autowired
    private ModelMapper mapper;

    public Nasabah convertToEntity(NasabahRequest request){
        return mapper.map(request, Nasabah.class);
    }

    public NasabahResponse convertToResponse(Nasabah nasabah){
        return mapper.map(nasabah, NasabahResponse.class);
    }

    @Override
    public List<NasabahResponse> findAll() {
        var nasabah = nasabahRepo.findAll().stream().filter(nasabah1 -> nasabah1.getIsDeleted()
        .equals(Boolean.FALSE)).collect(Collectors.toList());
        return nasabah.stream().map(this::convertToResponse).collect(Collectors.toList());
    }

    @Override
    public NasabahResponse create(NasabahRequest request) {
        var nasabah = convertToEntity(request);
        nasabah.setIsDeleted(Boolean.FALSE);
        return convertToResponse(nasabahRepo.save(nasabah));
    }

    @Transactional
    @Override // melakukan alur transfer antar rekening
    public void transfer(String norek1, String norek2, Double saldo){
        Nasabah nasabah1 = nasabahRepo.findByNorek(norek1);
        if(nasabah1==null){ // mengecek apakah nomor rekening nya valid atau tidak
            throw new RuntimeException("Rekening pertama tidak valid");
        }
        if(nasabah1.getAmount() < saldo){ // mengecek apakah saldo tersebut cukup atau tidak
            throw new RuntimeException("Saldo tidak mencukupi");
        }

        nasabah1.setAmount(nasabah1.getAmount() - saldo); // untuk mengurangi saldo yang ingin ditransfer
        nasabahRepo.save(nasabah1);

        Nasabah nasabah2 = nasabahRepo.findByNorek(norek2);
        if(nasabah2==null){
            throw new RuntimeException("Rekening kedua tidak valid");
        }
        nasabah2.setAmount(nasabah2.getAmount() + saldo); // untuk menambahkan saldo dari rekening sebelumnya
        nasabahRepo.save(nasabah2);
    }
}
