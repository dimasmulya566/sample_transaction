package com.sample.transaction.sample_transaction.service;

import com.sample.transaction.sample_transaction.dto.NasabahRequest;
import com.sample.transaction.sample_transaction.dto.NasabahResponse;

import java.util.List;

public interface NasabahService {
    List<NasabahResponse> findAll();
    NasabahResponse create(NasabahRequest request);
    public void transfer(String norek1, String norek2, Double saldo);
}
