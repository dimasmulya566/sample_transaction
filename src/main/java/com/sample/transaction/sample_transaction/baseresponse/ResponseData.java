package com.sample.transaction.sample_transaction.baseresponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData<T>{
    private Boolean status;
    private String message;
    private T payload;
}
