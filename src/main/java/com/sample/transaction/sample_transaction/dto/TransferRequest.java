package com.sample.transaction.sample_transaction.dto;

import lombok.Data;

@Data
public class TransferRequest {
    private String noRek1;
    private String noRek2;
    private Double amount;
}
