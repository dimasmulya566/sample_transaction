package com.sample.transaction.sample_transaction.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class NasabahRequest {

    private String name;
    private String norek;
    private Double amount;
    private LocalDateTime date;

}
