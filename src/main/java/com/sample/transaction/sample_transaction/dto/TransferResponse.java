package com.sample.transaction.sample_transaction.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferResponse {
    private String noRek1;
    private String noRek2;
    private Double amount;
}
