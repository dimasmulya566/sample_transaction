package com.sample.transaction.sample_transaction.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NasabahResponse {

    private Long id;
    private String name;
    private String norek;
    private Double amount;
    private LocalDateTime date;
}
