package com.sample.transaction.sample_transaction.controller;

import com.sample.transaction.sample_transaction.baseresponse.ResponseData;
import com.sample.transaction.sample_transaction.dto.NasabahRequest;
import com.sample.transaction.sample_transaction.dto.NasabahResponse;
import com.sample.transaction.sample_transaction.dto.TransferRequest;
import com.sample.transaction.sample_transaction.service.NasabahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/")
public class NasabahController {

    @Autowired
    private NasabahService service;

    @GetMapping
    public List<NasabahResponse> findAll(){
        return service.findAll();
    }

    @PostMapping
    public ResponseEntity<ResponseData<Object>> create(@RequestBody NasabahRequest request){
        ResponseData responseData = new ResponseData<>();
        responseData.setStatus(true);
        responseData.setMessage("DONE");
        responseData.setPayload(service.create(request));
        return ResponseEntity.ok(responseData);
    }

    @PostMapping("transfer")
    public void transfer(@RequestBody TransferRequest request){
        service.transfer(request.getNoRek1(), request.getNoRek2(), request.getAmount());
    }
}
