package com.sample.transaction.sample_transaction.repo;

import com.sample.transaction.sample_transaction.model.Nasabah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NasabahRepo extends JpaRepository<Nasabah, Long> {
    // mencari berdasarkan nomor rekening tersebut
    public Nasabah findByNorek(String norek);
}
